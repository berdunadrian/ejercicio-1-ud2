package mostrandodatos;

import java.util.Scanner;

public class Mostrandodatos {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		System.out.print("Introduzca su nombre: ");
		String nombre = in.nextLine();
		System.out.print("\nIntroduzca su apellido: ");
		String apellido = in.nextLine();
		
		System.out.println("Su nombre es: "+nombre+" "+apellido);
		in.close();
	}

}
